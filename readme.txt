Curso de git 
ESCREVER NO TERMINAL E VIM name_arquivo
letra i 
:wq

========CICLO DO GIT=========
No git existem 4 estagios dentro de um ciclo...
Primeiro estágio se chama untracked, na qual o git ele não reconhece o arquivo dentro de seu repositório
Segundo estágio se chama unmodified na qual o arquivo não foi editado
Terceiro estágio se chama modified na qual o arquivo foi modificado 
Quarto estágio se chama staged na qual você direciona a versão do push do git

Comandos dentro do ciclo:
git status
git add nome_arquivo ou * (adiciona tudo)
git commit -m "meu texto de explicacao"
git push 
git pull
git log
git diff
git reset HEAD nome_arquivo
git stash 
git revert minha_id_do_log

status = verifica.
add = adiciona para um pacote onde vai receber o commit. 
commit = comentario para a versão (recomendado sempre dizer o que foi feito).
push = enviando a versão para o repositório.
pull = baixar a versão do repositório. 
log = saber o histórico.
log --decorate = saber quais tag, branch e assim vai.
diff = mostra o que você modificou (muito importante).
reset --soft, --mixed e --hard = ele exclui da fila do commit, soft exclui, mixed ele volta a linha, hard exclui geral e so submit o que não foi resetado.
stash = guarda temporario como um pause em seu trabalho no arquivo.
stash apply = ele aplica e volta para o commit.
stash list = lista todas as stash.
revert = ele volta para a versão anterior baseado no log e não no clone ou versão do push.

===BRANCH===
Branch ele é caminhos que se cria em paralelo com o master, depois também é possivel mesclar.

git checkout -b nome_branck = Cria uma branch.
git branch = lista todos os branch.
git checkout nome_da_branch = entra na branch que deseja.
git branch -D nome_da_branch = deleta a branch.
git merge nome_da_branch = você mescla e depois é so dar git push.